# Notice Banner Workflow 😇 🧙‍ 😇

## Clone Entire Repository
Ensure GIT has been set up on your operating system and access the repo on your bitbucket account. 

1. Go to bitbucket and search for the repo tagmanager-notice-banner
2. Download/clone the entire repo onto your local operating system.

## Repo Set-up
Ensure all contents of the repo have been transffered locally. 

1. Navigate to the tagmanager-notice-banner directory on your local machine and open directory in your finder. 
2. The files and folders you will be copying live in: 
    ```
    -clients
        -client_name
    ```
3. Remember this directory is the baseplate for all notice banners going forward. Please do not overwrite. 
4. Create a new client directory e.g tregenna and paste the contents of the baseplate into that directory. Your folder structure should look like this: 
    ```
    -clients
        -tregenna
            - src
            - package.json
            - webpack.common.js
            - webpack.dev.js
            - webpack.prod.js
    ``` 

## Development Environment - Initiate Project Locally
Steps below explain how to run the project locally for development/testing purposes. 

1. At the ROOT of the client directory you have just created run the command in your terminal ```npm run install``` this will install a ```node_modules``` directory in the root of your client folder. To test run the command in terminal ```npm run start```. This will spin up a local dev server on your machine. 
2. In your text editor you will see two directories within your client directory that you will only need to be concerned with. SRC and DIST. 
3. You're development components site in the SRC directory. Here you can amend the templated source code. 

## Working with AppData.js
This AppData.js file is the foundation of where the content lives. The files has a number of key value pairs that dynamically pull through to the popup when content is edited. 

**KEYS** - The key properties should never be changed. 👿   ONLY the **VALUES** that are assigned to them. 😎

**VALUES** - Ensure your keep to the data type as the template. 🤓

## Production Environment - Initiate Project for Tag Manager 
**Terminal Commands** - Navigate to your terminal and run the command ```npm run build```. This will create a production file in the ```dist``` directory. The code is minified and compiled to all be inline to make it compatible for tagmanger. The contents of the popup will be in ```index.html```. This is the file you need to transfer to Tag Manger. 

**Code to remove** - Ensure before coping code from ```index.html``` file the following HTML tags are removed: 

Once these are removed you're ready to paste over to Tag Manger. 🤩

## 🤠 💃

## Version Control
Once you have finished your work we will need to version control it to keep it nice and safe. To do so go to your terminal navigate to the root of the tagmanager notice banner directory and do the following: 

1. Pull exisiting work onto your machine 
```
git pull
```

2. Add you work to the master directory

```
git add . 
```

3. Commit your work to the master directory

```
git commit -m "write message here." 
```

Be sure to write a detailed commit message as you can so when others pull in your work thet will know what you've done. 

4. Push work to remote repo

```
git push
```