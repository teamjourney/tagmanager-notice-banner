export const appData = [
    {
        linkAttributes: {
            url: 'https://www.calcotcollection.co.uk/coronavirus-update/', 
            target: '_blank'
        },
        noticeContent: {
            headingText: 'Hotel Closure:',
            dropText: 'We are now closed from 21st March until further notice.',
            mobileText: 'Please visit our FAQs page for more information.',
            buttonText: 'Learn more',
            exitButton: 'Dismiss'
        }
    }
]

export const pageTargetClass = 'body--page'

//When to display banner: 1000 = 1s
export const cookieTimer = 2000

//Name of Cookie
export const cookieName = 'notice-cookie'

// 0.5 = 12 hours
export const cookieSessionEnd = new Date(new Date().getTime() + 60 * 60 * 1000);