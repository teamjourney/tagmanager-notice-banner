import * as Cookies from "js-cookie";
import { cookieTimer, pageTargetClass, cookieName,  } from './AppData'

class CookieSession {
    constructor() {
        this.events()
    }

    events() {
        const noticeCookie = Cookies.get(`${cookieName}`),
            body = document.getElementsByTagName("body")[0],
            deactivateBtn = document.querySelector('.notice-banner__button'),
            promotionLink = document.querySelector('.notice-banner__text')
        
        setTimeout(() => {
            if(document.body.classList.contains(`${pageTargetClass}`)) {
                if(noticeCookie) {
                    body.classList.add('notice--has-shown');
                    body.classList.remove('notice--is-visible');
                } else {
                    setTimeout(() => {
                        body.classList.add('notice--is-visible');
                    }, `${cookieTimer}`)
    
                    deactivateBtn.addEventListener('click', function() {
                        Cookies.set(`${cookieName}`, true);
                        body.classList.remove('notice--is-visible')
                        body.classList.add('notice--has-shown')
                    })
    
                    promotionLink.addEventListener('click', function() {
                        Cookies.set(`${cookieName}`, true);
                        body.classList.remove('notice--is-visible');
                        body.classList.add('notice--has-shown')
                    })
                }
            } else {
                body.classList.add('notice--has-shown');
            }
        }, 3000);
    }
}

export default CookieSession