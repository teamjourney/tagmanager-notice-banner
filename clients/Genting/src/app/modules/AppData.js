export const appData = [
    {
        linkAttributes: {
            url: 'https://www.gentinghotel.co.uk/coronavirusupdate', 
            target: '_self'
            // idKey: 'Notice_Click_Through'
        },
        noticeContent: {
            headingText: 'We remain closed for hotel guests while in tier 3.',
            dropText: 'Our Santai Spa and Leisure Club are now open.',
            mobileText: 'More info:',
            buttonText: 'here'
        }
    }
]

export const pageTargetClass = 'body--notice-activator'

export const cookieTimer = 5000

export const cookieName = 'notice-cookie'

export const cookieSessionEnd = new Date(new Date().getTime() + 60 * 60 * 1000);