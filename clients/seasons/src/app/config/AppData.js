export const appData = [
    {
        linkAttributes: {
            url: 'https://www.seasonsholidays.com/coronavirus-update/', 
            target: '_self'
        },
        noticeContent: {
            headingText: 'COVID Update:',
            dropText: '',
            mobileText: ' We\'ve introduced some new measures across all of our resorts.',
            buttonText: 'Find out more',
            exitButton: 'Dismiss'
        }
    }
]

export const pageTargetClass = 'body--page'

//When to display banner: 1000 = 1s
export const cookieTimer = 2000

//Name of Cookie
export const cookieName = 'notice-cookie'

// 0.5 = 12 hours
export const cookieSessionEnd = new Date(new Date().getTime() + 60 * 60 * 1000);