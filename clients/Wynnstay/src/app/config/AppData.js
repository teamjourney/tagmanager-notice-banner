export const appData = [
    {
        linkAttributes: {
            url: 'https://www.wynnstayhotel.com/coronavirus-update/', 
            target: '_self'
        },
        noticeContent: {
            headingText: 'Hotel Closure Update due to COVID-19',
            dropText: '',
            mobileText: '',
            buttonText: 'Find out more',
            exitButton: 'Dismiss'
        }
    }
]

export const pageTargetClass = 'body--page'

//When to display banner: 1000 = 1s
export const cookieTimer = 5000

//Name of Cookie
export const cookieName = 'notice-cookie'

// 0.5 = 12 hours
export const cookieSessionEnd = new Date(new Date().getTime() + 60 * 60 * 1000);