export const appData = [
    {
        linkAttributes: {
            url: 'https://google.com', 
            target: '_self'
        },
        noticeContent: {
            headingText: 'Hotel closure:',
            dropText: 'We are now closed from 21st March until further notice.',
            mobileText: 'Please visit our FAQs page for more information.',
            buttonText: 'Find out more',
            exitButton: 'Dismiss'
        }
    }
]

export const pageTargetClass = 'body--page'

//When to display banner: 1000 = 1s
export const cookieTimer = 3000

//Name of Cookie
export const cookieName = 'notice-cookie'

// 0.5 = 12 hours
export const cookieSessionEnd = new Date(new Date().getTime() + 60 * 60 * 1000);