export const appData = [
    {
        linkAttributes: {
            url: '', 
            target: '_self'
        },
        noticeContent: {
            headingText: 'COVID19 Update:',
            dropText: 'To ensure the safety and wellbeing of guests and staff,',
            mobileText: 'we’ll be closing our health club and spa from 31st March for a maximum of 4 weeks. Thank you for your understanding.',
            buttonText: '',
            exitButton: 'Dismiss'
        }
    }
]

export const pageTargetClass = 'body--page'

//When to display banner: 1000 = 1s
export const cookieTimer = 5000

//Name of Cookie
export const cookieName = 'notice-cookie'

// 0.5 = 12 hours
export const cookieSessionEnd = new Date(new Date().getTime() + 60 * 60 * 1000);