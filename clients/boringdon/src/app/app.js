import { appData } from '../app/config/AppData'

document.getElementById('app-notice').innerHTML = `
  ${appData.map(function(data) {

  const dataQueriesContent = data.noticeContent, 
        dataQueriesLinkAttributes = data.linkAttributes

        
  const headingText = dataQueriesContent.headingText,
        dropText = dataQueriesContent.dropText,
        mobileText = dataQueriesContent.mobileText,
        buttonText = dataQueriesContent.buttonText,
        url = dataQueriesLinkAttributes .url,
        target = dataQueriesLinkAttributes .target,   
        exitButton = dataQueriesContent.exitButton

    return `
    <div class="notice-banner">
      <div class="notice-banner__container">
        <div class="notice-banner__inner">
          <div class="notice-banner__message">
            <div class="notice-banner__text">
              <p><strong>${headingText}</strong> <span>${dropText}</span> ${mobileText} <a href="${url}" target="${target}" class="banner-deactivator">${buttonText}</a></p>
            </div>
          </div>
          <div class="notice-banner__button">
            <button class="notice-banner__close">${exitButton}</button>
          </div>
        </div>
      </div>
    </div>
    `
  })}
`

import CookieSession from './modules/CookieSession'

new CookieSession()