import { appData } from './modules/AppData'

document.getElementById('app-notice').innerHTML = `
  ${appData.map(function(data) {
        
  const headingText = data.noticeContent.headingText;
  const dropText = data.noticeContent.dropText;
  const mobileText = data.noticeContent.mobileText;
  const buttonText = data.noticeContent.buttonText;
  const url = data.linkAttributes.url;
  const target = data.linkAttributes.target;

    return `
    <div class="notice-banner">
      <div class="notice-banner__container">
        <div class="notice-banner__inner">
          <div class="notice-banner__message">
            <div class="notice-banner__text">
              <p><strong>${headingText}</strong> <span>${dropText}</span> ${mobileText} <a href="${url}" target="${target}">${buttonText}</a></p>
            </div>
          </div>

          <div class="notice-banner__button">
            <button class="notice-banner__close">Dismiss</button>
          </div>
        </div>
      </div>
    </div>
    `
  })}
`

import CookieSession from './modules/CookieSession'

new CookieSession()

import DeactivateActions from './modules/DeactivateActions'

const popupExit = new DeactivateActions(
  '.notice-banner__close', 
  '.notice-banner', 
  'notice-banner--is-deactivated'
)

// New object instance for deactivating popup when triggering site modal/booking engine (remove comments to initiate).

// const popupDeactivate = new DeactivateActions(
//   '.notice-banner', 
//   '.notice-banner', 
//   'notice-banner--is-deactivated'
// )

setTimeout(() => { 
  document.body.classList.add('body--notice-activator')
}, 1000);