import * as Cookies from "js-cookie";
import { cookieTimer, pageTargetClass } from './AppData'

class CookieSession {
    constructor() {
        this.events()
    }

    events() {
        const noticeCookie = Cookies.get('notice-cookie');
        const body = document.getElementsByTagName("body")[0];
        setTimeout(() => {
            if(document.body.classList.contains(`${pageTargetClass}`)) {
                
                    setTimeout(() => {
                        body.classList.add('notice--is-visible');
                    }, `${cookieTimer}`)

                    setTimeout(() => {
                        Cookies.set('notice-cookie', true);
                    }, `${cookieTimer}`)
            } 
        }, 3000);
    }
}

export default CookieSession