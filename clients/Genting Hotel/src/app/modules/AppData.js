export const appData = [
    {
        linkAttributes: {
            url: 'https://www.gentinghotel.co.uk/coronavirusupdate', 
            target: '_self'
            // idKey: 'Notice_Click_Through'
        },
        noticeContent: {
            headingText: 'Coronavirus Update:',
            dropText: 'We\'re pleased to confirm we are reopening on 17th August.',
            mobileText: 'For more information and to book now',
            buttonText: 'see here'
        }
    }
]

export const pageTargetClass = 'body--notice-activator'

export const cookieTimer = 5000