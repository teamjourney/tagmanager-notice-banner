# Pop-up Webpack Workflow 😇 🧙‍ 😇

## Clone Entire Repository
Ensure GIT has been set up on your operating system and access the repo on your bitbucket account. 

```text
1. Go to bitbucket and search for the repo tagmanager-workflow.
2. Download/clone the entire repo onto your local operating system.
```

## Repo Set-up
Ensure all contents of the repo have been transffered locally. 

```
1. Navigate to the tagmanager-workflow directory on your local machine and open directory in your finder. 
2. Copy all files and create new directory for the specified client you're creating the popup for. 
3. Paste all files copyed from the tagmanger-workflow repo into your new directory.
4. Open terminal `hit the spacebar and search for terminal`. Navigate to the ROOT of the directory you created with the pasted contents of the tagmanger-workflow directory.
5. Run the command `npm install` - The install will take couple of minutes to download all projext dependencies. 
```

## Development Environment - Initiate Project Locally
Steps below explain how to run the project locally for development/testing purposes. 

```
1. At the ROOT of your directory run the command npm run start. This will spin up a local dev server on your machine. 
2. In your text editor you will see two directories that you will only need to be concerned with. SRC and DIST. 
3. You're development components site in the SRC directory. Here you can amend the templated source code. 
```

## Working with AppData.js
This AppData.js file is the foundation of where the popup content lives. The files has a number of key value pairs that dynamically pull through to the popup when content is edited. 

**KEYS** - The key properties should never be changed. 👿   ONLY the **VALUES** that are assigned to them. 😎

**VALUES** - Ensure your keep to the data type as the template. 🤓


## Production Environment - Initiate Project for Tag Manager 
**Terminal Commands** - Navigate to your terminal and run the command ```npm run build```. This will create a production file in the ```dist``` directory. The code is minified and compiled to all be inline to make it compatible for tagmanger. The contents of the popup will be in ```index.html```. This is the file you need to transfer to Tag Manger. 

**Code to remove** - Ensure before coping code from ```index.html``` file the following HTML tags are removed: 

```
<head>
```
Once these are removed you're ready to paste over to Tag Manger. 🤩

## 🤠 💃