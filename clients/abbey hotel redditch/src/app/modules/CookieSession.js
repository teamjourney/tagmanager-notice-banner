import * as Cookies from "js-cookie";
import { cookieTimer, pageTargetClass, cookieName, cookieSessionEnd } from '../config/AppData'

class CookieSession {
    constructor() {
        this.events()
    }

    events() {
        const noticeCookie = Cookies.get(`${cookieName}`),
            body = document.getElementsByTagName("body")[0],
            cookieTimeOut = cookieSessionEnd,
            bannerCloseBtn = document.querySelector('.notice-banner__close'), 
            noticeBanner = document.querySelector('.notice-banner'),
            bannerLink = document.querySelector('.banner-deactivator')

        if(document.body.classList.contains(`${pageTargetClass}`)) {
            if(noticeCookie) {
                body.classList.add('notice--has-shown');
                body.classList.remove('notice--is-visible');
            } else {
                setTimeout(() => {
                    body.classList.add('notice--is-visible');
                }, `${cookieTimer}`)

                bannerCloseBtn.addEventListener('click', function() {
                    Cookies.set(`${cookieName}`, true, {
                        expires: cookieTimeOut
                    });
                    body.classList.remove('notice--is-visible');
                    body.classList.add('notice--has-shown')
                    noticeBanner.classList.add('notice-banner--is-deactivated')
                })

                bannerLink.addEventListener('click', function() {
                    Cookies.set(`${cookieName}`, true, {
                        expires: cookieTimeOut
                    });
                    body.classList.remove('notice--is-visible');
                    body.classList.add('notice--has-shown')
                    noticeBanner.classList.add('notice-banner--is-deactivated')
                })
            }
        } else {
            body.classList.add('notice--has-shown');
        }
    }
}

export default CookieSession