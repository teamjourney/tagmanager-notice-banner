export const appData = [
    {
        linkAttributes: {
            url: 'https://www.theabbeyhotel.co.uk/covid-19-update/', 
            target: '_self'
        },
        noticeContent: {
            headingText: 'COVID19 Update:',
            dropText: 'We, at The Abbey Hotel, have taken the decision to temporarily close our doors as of Tuesday 24th March.',
            mobileText: 'Please click here to find out more.',
            buttonText: 'Learn more',
            exitButton: 'Dismiss'
        }
    }
]

export const pageTargetClass = 'body--page'

//When to display banner: 1000 = 1s
export const cookieTimer = 5000

//Name of Cookie
export const cookieName = 'notice-cookie'

// 0.5 = 12 hours
export const cookieSessionEnd = new Date(new Date().getTime() + 60 * 60 * 1000);