export const appData = [
    {
        linkAttributes: {
            url: 'https://www.abbeyhotelbath.co.uk/coronavirus-update/', 
            target: '_self'
        },
        noticeContent: {
            headingText: 'COVID19 Update:',
            dropText: 'We are now temporarily closed.',
            mobileText: 'Please visit our coronavirus update page for more information.',
            buttonText: 'Learn more',
            exitButton: 'Dismiss'
        }
    }
]

export const pageTargetClass = 'body--page'

//When to display banner: 1000 = 1s
export const cookieTimer = 5000

//Name of Cookie
export const cookieName = 'notice-cookie'

// 0.5 = 12 hours
export const cookieSessionEnd = new Date(new Date().getTime() + 60 * 60 * 1000);