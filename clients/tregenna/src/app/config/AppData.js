export const appData = [
    {
        linkAttributes: {
            url: 'https://www.tregenna-castle.co.uk/faqs/', 
            target: '_self'
        },
        noticeContent: {
            headingText: 'COVID19 Update:',
            dropText: 'Entry to our Resort is unavailable until further notice.',
            mobileText: 'View our statement.',
            buttonText: 'Find out more',
            exitButton: 'Dismiss'
        }
    }
]

export const pageTargetClass = 'body--page'

//When to display banner: 1000 = 1s
export const cookieTimer = 5000

//Name of Cookie
export const cookieName = 'notice-cookie'

// 0.5 = 12 hours
export const cookieSessionEnd = new Date(new Date().getTime() + 60 * 60 * 1000);