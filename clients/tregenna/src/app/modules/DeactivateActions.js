class DeactivateActions {

    constructor(initDeactivationNode, nodeDeactivator, activatorDecloration) {
        this.initDeactivationNode = document.querySelector(initDeactivationNode)
        this.nodeDeactivator =  document.querySelector(nodeDeactivator)
        this.activatorDecloration = activatorDecloration
        this.events()
    }

    events() {
        this.initDeactivationNode.addEventListener('click', this.initDeactivators.bind(this))
    }

    initDeactivators() {
        this.nodeDeactivator.classList.add(this.activatorDecloration);
    }
}

export default DeactivateActions