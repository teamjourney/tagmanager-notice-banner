export const appData = [
    {
        linkAttributes: {
            url: '/terms/', 
            target: '_self'
        },
        noticeContent: {
            headingText: 'Booking policy update',
            dropText: '',
            mobileText: 'Plan ahead with a high degree of flexibility.',
            buttonText: 'Find out more',
            exitButton: 'Dismiss'
        }
    }
]

export const pageTargetClass = 'body--page'

//When to display banner: 1000 = 1s
export const cookieTimer = 2000

//Name of Cookie
export const cookieName = 'notice-cookie'

// 0.5 = 12 hours
export const cookieSessionEnd = new Date(new Date().getTime() + 60 * 60 * 1000);