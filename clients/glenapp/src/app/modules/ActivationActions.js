class ActivationActions {

    constructor(initActivationNode, nodeActivation, activatorDecloration, documentBody, activationNodeDisabledDecloration) {
        this.initActivationNode = document.querySelector(initActivationNode)
        this.nodeActivation =  document.querySelector(nodeActivation)
        this.activatorDecloration = activatorDecloration
        this.documentBody = document.getElementsByTagName(documentBody)[0]
        this.activationNodeDisabledDecloration = activationNodeDisabledDecloration
        this.events()
    }

    events() {
        this.initActivationNode.addEventListener('click', this.initActivationActions.bind(this))
    }

    initActivationActions(e) {
        e.preventDefault()
        this.nodeActivation.classList.add(this.activatorDecloration)
        this.documentBody.classList.add(this.activationNodeDisabledDecloration)
    }
}

export default ActivationActions