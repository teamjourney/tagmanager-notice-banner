export const appData = [
    {
        linkAttributes: {
            url: 'https://www.seaham-hall.co.uk/coronavirus-update/', 
            target: '_self'
        },
        noticeContent: {
            headingText: 'COVID19 Update:',
            dropText: 'We’re still open for business.',
            mobileText: 'Please visit our FAQs page for more information.',
            buttonText: 'Find out more',
            exitButton: 'Dismiss'
        }
    }
]

export const pageTargetClass = 'body--page'

//When to display banner: 1000 = 1s
export const cookieTimer = 5000

//Name of Cookie
export const cookieName = 'notice-cookie'

// 0.5 = 12 hours
export const cookieSessionEnd = new Date(new Date().getTime() + 60 * 60 * 1000);