export const appData = [
    {
        linkAttributes: {
            url: 'https://www.flatcaphotels.com/covid-19-update/', 
            target: '_self'
        },
        noticeContent: {
            headingText: 'COVID-19 has rocked the world;',
            dropText: 'For more information on how Flat Cap is helping fight the pandemic,',
            mobileText: 'please visit our latest updates page.',
            buttonText: 'Find out more',
            exitButton: 'Dismiss'
        }
    }
]

export const pageTargetClass = 'body--page'

//When to display banner: 1000 = 1s
export const cookieTimer = 3000

//Name of Cookie
export const cookieName = 'covid-notice-cookie'

// 0.5 = 12 hours
export const cookieSessionEnd = new Date(new Date().getTime() + 60 * 60 * 1000);