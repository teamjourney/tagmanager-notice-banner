export const appData = [
    {
        linkAttributes: {
            url: 'https://www.esplanadehotelnewquay.co.uk/covid-19/', 
            target: '_self'
        },
        noticeContent: {
            headingText: 'Hotel Closure:',
            dropText: 'In light of recent news, we’re sad to announce that we are closed until Friday 24th April.',
            mobileText: 'Please visit our FAQ’s page for more information.',
            buttonText: 'Learn more',
            exitButton: 'Dismiss'
        }
    }
]

export const pageTargetClass = 'body--page'

//When to display banner: 1000 = 1s
export const cookieTimer = 3000

//Name of Cookie
export const cookieName = 'notice-cookie'

// 0.5 = 12 hours
export const cookieSessionEnd = new Date(new Date().getTime() + 60 * 60 * 1000);