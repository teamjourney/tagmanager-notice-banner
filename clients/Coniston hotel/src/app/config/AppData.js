export const appData = [
    {
        linkAttributes: {
            url: 'https://www.theconistonhotel.com/tcs/covid-19-information/', 
            target: '_self'
        },
        noticeContent: {
            headingText: 'Covid-19 Update:',
            dropText: 'Information for our guests.',
            mobileText: 'Please click for more information',
            buttonText: 'Find out more',
            exitButton: 'Dismiss'
        }
    }
]

export const pageTargetClass = 'body--page'

//When to display banner: 1000 = 1s
export const cookieTimer = 5000

//Name of Cookie
export const cookieName = 'notice-cookie'

// 0.5 = 12 hours
export const cookieSessionEnd = new Date(new Date().getTime() + 60 * 60 * 1000);